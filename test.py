# -*- coding: utf-8 -*-

from urllib.parse import urlsplit
import time
import functools
import logging
import re

from scrapy.exceptions import NotConfigured
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import Rule

from reyhaneh.utils import douran_session
from reyhaneh.items import ReyhanehItem
from reyhaneh import settings
from .base import BaseSpider

logger = logging.getLogger(__name__)

class GeneralSpider(BaseSpider):
    name = 'general'
    __last_start_request = time.time()
    __configs = {}

    def __init__(self, *args, **kwargs):
        self.get_configs(True)
        self.start_url_redis_key = 'crawler:start_url:{}'.format(
            self.get_source())
        self.splash = self.get_configs().get('js_engine', False)
        super(GeneralSpider, self).__init__(*args, **kwargs)

    def parse_item(self, response, template):
        item = ReyhanehItem()
        item['meta'] = dict()
        uri_elem = response.xpath(template.get('uri_xpath'))

        if response.status in [404, 403]: #NO_CONTENT:
            uri_elem = response.url

        if uri_elem:
            if not isinstance(uri_elem, str):
                uri = uri_elem.extract_first()
            else:
                uri = uri_elem  #uri=url
            if template.get('uri_pattern'):
                uri_partial = re.search(template['uri_pattern'], uri)
                if uri_partial:
                    uri = uri_partial.group(1)
            uri = re.sub(r'-+', '', re.sub(r'/+', ':', uri)).strip(':')
            item['crawl_time'] = time.time()
            item['download_queue'] = self.get_configs().get('download_queue')
            item['download_size'] = len(response.body)
            item['is_user_template'] = template.get(
                'name', '').startswith('user')
            item['perma_link'] = response.url
            item['source'] = self.get_configs().get('source')
            item['uri'] = uri

            if response.status in [404, 403]:  # NO_CONTENT
                item['meta']['delete_time'] = [time.time()]
                item['meta']['error'] = ['Content Removed']
                douran_session.get(url='%s/api/DOURAN/crawlers/update/%s' % (settings.DASHBOARD_URL, settings.SID),
                                   params={'status': 'NO_NEW_CONTENT',
                                           'description': f'url <{response.url}> has no content'})

                logger.info(f'url {response.url} has no content')
            item['meta_config'] = template.get('configs', [])

            for conf in item['meta_config']:
                elems = response.xpath(conf.get('xpath')).extract()

                if not elems:
                    douran_session.get(url='%s/api/DOURAN/crawlers/update/%s' % (settings.DASHBOARD_URL, settings.SID),
                                       params={'status': 'ERROR',
                                               'description': f'meta "{conf.get("meta")}" with uri "{uri}", is invalid or changed. '})
                    logger.warning(f'Invalid meta({conf.get("meta")}): with {conf.get("xpath")}'
                                   f' on {response.url}')
                    continue

                item['meta'][
                    conf['meta']
                ] = (list(map(lambda x: x.strip(), elems)), conf)

            yield item
        else:
            self.logger.warning(f'Invalid uri {template.get("name")} :'
                                f' with {template.get("uri_xpath")} on {response.url}')

        if time.time() - self.__last_start_request > 3600:
            for req in self.start_requests():
                yield req

        return self.parse(response)

    def get_source(self):
        return self.get_configs().get('source')

    def get_request_expire_time(self):
        return self.get_configs().get('expire_time')

    def get_configs(self, force=False):
        if force or not self.__configs:
            try:
                url = '%s/api/DOURAN/crawlers/%d/configs' % (
                    settings.DASHBOARD_URL, settings.SID) #get configs url
                response = douran_session.get(url)
                if response.status_code != 200:
                    raise NotConfigured(response.status_code)
                self.__configs = response.json()
                self.__compile_configs(self.__configs)

            except Exception as err:
                raise NotConfigured(err)

        return self.__configs

    def __compile_configs(self, configs):
        GeneralSpider.download_delay = configs.get('download_delay', 0)
        config_start_urls = configs.get('start_urls', [])
        self.sitemap_urls = filter(
            lambda u: 'sitemap.xml' in u, config_start_urls)
        self.start_urls = filter(
            lambda u: 'sitemap.xml' not in u, config_start_urls)

        self.allow_domains = set(
            (urlsplit(url).netloc for url in config_start_urls)
        )

        rules = []
        for template in configs.get('crawl_templates', []):
            white_rules = filter(
                lambda x: x['white_list'], template.get('rules', []))
            black_rules = filter(
                lambda x: not x['white_list'], template.get('rules', []))

            def pluck_on_pattern(x): return x.get('pattern')
            callback = functools.partial(self.parse_item, template=template)

            rules.append(
                Rule(
                    LinkExtractor(
                        allow_domains=self.allow_domains,
                        allow=tuple(map(pluck_on_pattern, white_rules)),
                        deny=tuple(map(pluck_on_pattern, black_rules)),
                    ),
                    process_request=lambda req, _: req,
                    callback=callback,
                    follow=True)
            )

        self.rules = tuple(rules)

